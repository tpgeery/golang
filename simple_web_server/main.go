package main

import (
	"io"
	"net/http"
	"strconv"
	"strings"
)

func hello(w http.ResponseWriter, r *http.Request) {
	n := 1
	switch r.Method {
	case "GET":
		str := r.URL.Path
		str = strings.TrimPrefix(str, "/")
		m, err := strconv.Atoi(str)
		if err != nil {
			m = 1
		}
		n = m
	case "POST":
		if err := r.ParseForm(); err == nil {
			str := r.FormValue("n")
			m, err := strconv.Atoi(str)
			if err == nil {
				n = m
			}
		}
	}

	for i:=0; i<n; i++ {
		io.WriteString(w, "Hello World!")
	}
}

func main() {
	http.HandleFunc("/", hello)
	http.ListenAndServe(":8000", nil)
}
